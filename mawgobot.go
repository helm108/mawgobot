package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/helm108/mawgobot/src/bots"
	"log"
	"os"
)

// This type matches the signature of both bots.Local and bots.Netlify,
// meaning it can be used as an argument type.
type BotMaker func()

func RunBot(local BotMaker, netlify BotMaker, isNetlify bool) {
	if isNetlify {
		netlify()
	} else {
		local()
	}
}

func main() {
	isNetlify := os.Getenv("NETLIFY") == "true"

	// Only load the .env file locally.
	if !isNetlify {
		err := godotenv.Load()
		if err != nil {
			log.Fatal("Error loading .env file")
		}
	}

	RunBot(bots.Local, bots.Netlify, isNetlify)
}
