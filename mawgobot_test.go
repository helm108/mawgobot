package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var localCalled = false
var netlifyCalled = false

func localMock() {
	localCalled = true
}

func netlifyMock() {
	netlifyCalled = true
}

func setUp() {
	localCalled = false
	netlifyCalled = false
}

func TestDetectsNetlify(t *testing.T) {
	setUp()

	RunBot(localMock, netlifyMock, true)

	assert.False(t, localCalled, "Local called")
	assert.True(t, netlifyCalled, "Netlify not called")
}

func TestDefaultsToLocal(t *testing.T) {
	setUp()

	RunBot(localMock, netlifyMock, false)

	assert.False(t, netlifyCalled, "Netlify called")
	assert.True(t, localCalled, "Local not called")
}
