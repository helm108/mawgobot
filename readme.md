# MAWGoBot

A reimplementation of MAWBot and WAMBot in Go.

## Testing an MR

Create an MR for your branch. This will trigger a build in Netlify, resulting in the MAWGoBot function existing at the following URL:

    https://deploy-preview-${PREVIEW_NUMBER}--helm108-mawgobot.netlify.com/.netlify/functions/mawgobot

So for the branch `5-which-command` we get the URL https://deploy-preview-5--helm108-mawgobot.netlify.com/.netlify/functions/mawgobot.

_Note: I'm not sure if the number remains consistent with the issue number or if it just happens to be the fifth one right now._

Create a test bot in Telegram, and set the webhook as the deploy preview URL.

You'll need to update the environment variable to one containing your dev bot API key, and then remember to change it back when the feature is complete.

## Installing Project Dependencies

    go get ./...
    
## Running Locally

Create a filed called `.env` in the project root.

Inside this put

    LOCAL_BOT_TOKEN=yourdevbottoken
    
Run `go run mawgobot.go`

## Running Tests

    ./test.sh
    
This runs tests in all subdirs with coverage.

To browse the coverage data and find out exactly what is not covered by unit tests, run

    go tool cover -html=c.out

## Resources
[Netlify Go Example](https://github.com/netlify/aws-lambda-go-example)

https://core.telegram.org/bots/webhooks
curl -F "url=https://deploy-preview-2--helm108-mawgobot.netlify.com/.netlify/functions/mawgobot" https://api.telegram.org/bot<API_KEY>/setWebhook

# Go Tips
Run `go fmt` to format all Go files - ctrl+alt+shift+p in GoLand.
Run `go vet` to check for any code issue. 

# Commit Hooks
Run `./bin/install_githooks.sh` to set up our git hooks.

## pre-commit
This runs `go fmt`, `go vet` and `go test`. 
- `go fmt` formats the code to the Go standard and should not actually block the commit.
- `go vet` checks the code for any issues and exits with a failure if any problems are detected, blocking the commit.

Note: to run `go vet` on Linux you need to install `build-essential`:

    sudo apt-get update
    sudo apt-get install build-essential

