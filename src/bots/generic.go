package bots

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/helm108/mawgobot/src/processor"
	"gitlab.com/helm108/mawgobot/src/types"
	"log"
)

func ProcessResponse(bot *tgbotapi.BotAPI, telegramResponse types.TelegramResponse) {
	command := processor.ProcessTelegramResponse(telegramResponse)

	response := processor.ProcessCommand(command)

	if response.Text != "" {
		if bot.Debug {
			log.Printf("Reply: %s", response.Text)
		}

		msg := tgbotapi.NewMessage(telegramResponse.Message.Chat.ID, response.Text)
		if !response.IsBroadcast {
			msg.ReplyToMessageID = telegramResponse.Message.MessageID
		}

		if response.UseMarkdown {
			msg.ParseMode = "markdown"
		}
		msg.DisableWebPagePreview = response.DisableWebPagePreview

		_, newBotError := bot.Send(msg)
		if newBotError != nil {
			log.Panic(newBotError)
		}
	}
}
