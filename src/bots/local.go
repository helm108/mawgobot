package bots

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/helm108/mawgobot/src/types"
	"log"
	"os"
)

func convertMessageToTelegramResponse(update tgbotapi.Update) types.TelegramResponse {
	telegramFrom := types.TelegramFrom{
		ID:        update.Message.From.ID,
		IsBot:     update.Message.From.IsBot,
		FirstName: update.Message.From.FirstName,
		LastName:  update.Message.From.LastName,
		Username:  update.Message.From.UserName,
	}
	telegramChat := types.TelegramChat{
		ID:        update.Message.Chat.ID,
		FirstName: update.Message.Chat.FirstName,
		LastName:  update.Message.Chat.LastName,
		Username:  update.Message.Chat.UserName,
		Type:      update.Message.Chat.Type,
	}
	telegramMessage := types.TelegramMessage{
		MessageID: update.Message.MessageID,
		From:      telegramFrom,
		Chat:      telegramChat,
		Date:      update.Message.Date,
		Text:      update.Message.Text,
	}
	telegramResponse := types.TelegramResponse{
		UpdateId: update.UpdateID,
		Message:  telegramMessage,
	}

	return telegramResponse
}

func Local() {
	log.Print("Running Local Bot")

	botToken := os.Getenv("LOCAL_BOT_TOKEN")

	bot, newBotError := tgbotapi.NewBotAPI(botToken)
	if newBotError != nil {
		log.Panic(newBotError)
	}

	log.Printf("Clearing Webhook from %s", bot.Self.UserName)
	_, clearWebhookError := bot.RemoveWebhook()
	if clearWebhookError != nil {
		log.Panic(clearWebhookError)
	}

	bot.Debug = true

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, getUpdateError := bot.GetUpdatesChan(u)
	if getUpdateError != nil {
		log.Print(getUpdateError)
	}

	log.Print("Listening for updates.")
	for update := range updates {
		if update.Message == nil {
			continue
		}

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)
		telegramResponse := convertMessageToTelegramResponse(update)

		ProcessResponse(bot, telegramResponse)
	}
}
