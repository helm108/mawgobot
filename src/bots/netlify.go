package bots

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/helm108/mawgobot/src/data"
	"log"
	"os"
)

func handler(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	// Convert request JSON.
	telegramResponse := data.ParseJson(request.Body)

	// Create a Bot.
	bot, err := tgbotapi.NewBotAPI(os.Getenv("TELEGRAM_BOT_API_KEY"))
	if err != nil {
		log.Panic(err)
	}

	// Process bot response.
	ProcessResponse(bot, telegramResponse)

	return &events.APIGatewayProxyResponse{
		StatusCode: 200,
		Body:       "blot",
	}, nil
}

func Netlify() {
	lambda.Start(handler)
}
