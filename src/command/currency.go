package commands

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/helm108/mawgobot/src/types"
	"io"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
)

const CurrencyAPI = `http://api.exchangeratesapi.io/v1/latest`
const BitcoinAPI = `https://api.coindesk.com/v1/bpi/currentprice/USD.json`

// ValueRegex Fancy Regex used to parse user queries
// Capture groups:
// 1: leading currency symbol, one of either $, £ or € -- ([$£€]{0,1})
// 2: value, at least one digit -- ([\d\.]+)
// as much whitespace as you want -- \s*
// 3: currency code -- (\w*)
const ValueRegex = `(?P<symbol>[$£]{0,1})(?P<value>[\d.,]+)(?P<postsymbol>[€]{0,1})\s*(?P<code>\w*)`

type RatesQuery struct {
	OriginalCurrency string  // originally queried for
	DerivedCurrency  string  // used for calculations
	OriginalValue    float64 // originally queried for
	DerivedValue     float64 // used for calculations
	CurrencyStream   io.ReadCloser
	CurrencyRates    types.CurrencyRates
	BitcoinStream    io.ReadCloser
	BitcoinRate      types.BitcoinRate
	CombinedRates    map[string]float64
}

func NewRatesQuery(currency string, value float64) *RatesQuery {
	query := new(RatesQuery)
	query.OriginalCurrency = currency
	query.OriginalValue = value
	return query
}

func (query *RatesQuery) IsBitcoinQuery() bool {
	return query.OriginalCurrency == "BTC"
}

func hitAPI(url string) (io.ReadCloser, error) {
	resp, httpGetRequestError := http.Get(url)
	if httpGetRequestError != nil {
		return nil, httpGetRequestError
	}
	return resp.Body, nil
}

func (query *RatesQuery) doQuery() error {
	if !isValidCurrency(query.OriginalCurrency) {
		return errors.New(fmt.Sprintf("%s is not a supported currency", query.OriginalCurrency))
	}

	var currencyAPIUrl string
	if query.IsBitcoinQuery() {
		// Look up rates as if we queried for USD
		currencyAPIUrl = fmt.Sprintf("%s?base=%s", CurrencyAPI, "USD")
	} else {
		currencyAPIUrl = fmt.Sprintf("%s?base=%s", CurrencyAPI, query.OriginalCurrency)
	}

	currencyAPIUrl += "&access_key=" + os.Getenv("EXCHANGE_RATES_API_KEY")

	currencyStream, currencyAPIUnavailableError := hitAPI(currencyAPIUrl)
	if currencyAPIUnavailableError != nil {
		return currencyAPIUnavailableError
	}
	query.CurrencyStream = currencyStream

	bitcoinStream, bitcoinAPIUnavailableError := hitAPI(BitcoinAPI)
	if bitcoinAPIUnavailableError != nil {
		defer query.CurrencyStream.Close()
		return bitcoinAPIUnavailableError
	}
	query.BitcoinStream = bitcoinStream

	return nil
}

func (query *RatesQuery) decode() error {
	currencyDecoder := json.NewDecoder(query.CurrencyStream)
	currencyStreamDecodingError := currencyDecoder.Decode(&query.CurrencyRates)
	if currencyStreamDecodingError != nil {
		return currencyStreamDecodingError
	}

	bitcoinDecoder := json.NewDecoder(query.BitcoinStream)
	bitcoinStreamDecodingError := bitcoinDecoder.Decode(&query.BitcoinRate)
	if bitcoinStreamDecodingError != nil {
		return bitcoinStreamDecodingError
	}

	query.CombinedRates = query.CurrencyRates.Rates
	if query.IsBitcoinQuery() {
		// Set the query's value to be x times that of the USD/BTC exchange rate
		query.DerivedValue = query.OriginalValue * query.BitcoinRate.BPI.USD.Rate
	} else {
		// Derive the BTC rate for BaseCurrency from the available USD rate for BTC
		query.DerivedValue = query.OriginalValue
		query.CombinedRates["BTC"] = query.CombinedRates["USD"] / query.BitcoinRate.BPI.USD.Rate
	}
	return nil
}

func (query *RatesQuery) closeStreams() {
	defer query.CurrencyStream.Close()
	defer query.BitcoinStream.Close()
}

func getSymbol(currency string) (string, bool, error) {
	// returns the symbol, whether the symbol precedes the number, or an error
	switch currency {
	case "AUD":
		fallthrough
	case "USD":
		return "$", true, nil
	case "GBP":
		return "£", true, nil
	case "EUR":
		return "€", false, nil
	default:
		return "", false, errors.New(fmt.Sprintf("No currency symbol for %s", currency))
	}
}

func getCurrency(symbol string) (string, error) {
	switch symbol {
	case "$":
		return "USD", nil
	case "£":
		return "GBP", nil
	case "€":
		return "EUR", nil
	default:
		return "", errors.New(fmt.Sprintf("No currency has %s as a symbol", symbol))
	}
}

func isValidCurrency(currencyCode string) bool {
	// The currencies we can construct queries for
	var availableCurrencies = [...]string{
		"EUR", "IDR", "BGN", "ILS", "GBP", "DKK", "CAD", "JPY", "HUF", "RON",
		"MYR", "SEK", "SGD", "HKD", "AUD", "CHF", "KRW", "CNY", "TRY", "HRK",
		"NZD", "THB", "USD", "NOK", "RUB", "INR", "MXN", "CZK", "BRL", "PLN",
		"PHP", "ZAR", "ISK", "BTC",
	}
	for _, code := range availableCurrencies {
		if currencyCode == code {
			return true
		}
	}
	return false
}

func parseArgs(args string) (string, float64, error) {
	if len(args) <= 0 {
		return "", 0.0, errors.New("no args provided")
	}

	regex := regexp.MustCompile(ValueRegex)
	matches := regex.FindStringSubmatch(args)

	var symbol string
	var postSymbol string
	var currencyCode string
	var value float64

	for i, name := range regex.SubexpNames() {
		switch name {
		case "symbol":
			symbol = matches[i]
		case "postsymbol":
			postSymbol = matches[i]
		case "value":
			valueWithoutCommas := strings.Replace(matches[i], ",", "", -1)
			value, _ = strconv.ParseFloat(valueWithoutCommas, 64)
		case "code":
			currencyCode = strings.ToUpper(matches[i])
		}
	}

	if symbol != "" {
		symbolCurrencyCode, symbolHasNoCurrencyErr := getCurrency(symbol)
		if symbolHasNoCurrencyErr != nil {
			return "", 0.0, symbolHasNoCurrencyErr
		} else {
			currencyCode = symbolCurrencyCode
		}
	} else if postSymbol != "" {
		postSymbolCurrencyCode, postSymbolHasNoCurrencyErr := getCurrency(postSymbol)
		if postSymbolHasNoCurrencyErr != nil {
			return "", 0.0, postSymbolHasNoCurrencyErr
		} else {
			currencyCode = postSymbolCurrencyCode
		}
	}

	if !isValidCurrency(currencyCode) {
		var errmsg string
		if currencyCode != "" {
			errmsg = fmt.Sprintf("%s is not a supported currency", currencyCode)
		} else {
			errmsg = fmt.Sprintf("Could not understand your query")
		}
		return "", 0.0, errors.New(errmsg)
	}

	return currencyCode, value, nil
}

// Super awesome recursive function
// Separates the incoming string into groups of three chars, up until the first
// occurrence of the '.' char. Thus, "12345.67" -> "12,345.67"
func getThousandsSeparatedNumber(number string) (int, string) {
	if len(number) == 0 {
		return 0, ""
	}

	var head byte = number[0]
	var tail string = number[1:]
	if head == '.' {
		// We found a period. Halt and return the whole tail.
		return 0, string(head) + tail
	}

	groupVal, tailResult := getThousandsSeparatedNumber(tail)
	if groupVal == 3 {
		tailResult = string(head) + "," + tailResult
		return 1, tailResult
	} else {
		return groupVal + 1, string(head) + tailResult
	}
}

// GetCurrencyDescription Given a currency code and a float, this function returns its description,
// and the index of the period symbol in that description (for easier padding).
//
// E.g.,
// "SEK", 15.6 --> ("15.60 SEK", 2)
// "GBP", 100000.0001 --> ("£100,000.00", 8)
// "EUR", 100000.0111 --> ("100,000.01€", 7)
func GetCurrencyDescription(currencyCode string, value float64) (string, int) {
	symbol, symbolPrecedesNumber, currencyHasNoSymbolErr := getSymbol(currencyCode)
	numberRepresentation := fmt.Sprintf("%.2f", value)
	_, numberRepresentation = getThousandsSeparatedNumber(numberRepresentation)
	periodIndex := strings.Index(numberRepresentation, ".")

	if currencyHasNoSymbolErr == nil {
		// the currency has a symbol
		if symbolPrecedesNumber {
			return symbol + numberRepresentation, periodIndex + 1
		} else {
			return numberRepresentation + symbol, periodIndex
		}
	} else {
		// the currency has no symbol
		return numberRepresentation + " " + currencyCode, periodIndex
	}
}

func writeWithPadding(toWrite string, paddingCount int) string {
	var result strings.Builder
	result.WriteString("`")
	for j := 0; j < paddingCount; j++ {
		result.WriteString(" ")
	}
	result.WriteString(toWrite)
	result.WriteString("`")
	return result.String()
}

func (query *RatesQuery) getDescription() string {
	// The currencies we desire our results to be presented in
	var currenciesToWatch = []string{
		"SEK", "GBP", "EUR", "USD", "BTC", "AUD",
	}

	var currenciesToPrint []string
	for _, curr := range currenciesToWatch {
		// Don't print the currency we requested a query for...
		if curr != query.OriginalCurrency {
			currenciesToPrint = append(currenciesToPrint, curr)
		}
	}

	var description strings.Builder
	descriptions := make([]string, len(currenciesToPrint))
	periodIndices := make([]int, len(currenciesToPrint))
	maxPeriodIndex := 0

	for i, curr := range currenciesToPrint {
		// For each currency to present, figure out where the period symbol is,
		// so that we can apply the right amount of left padding
		val := query.CurrencyRates.Rates[curr] * query.DerivedValue
		description, periodIndex := GetCurrencyDescription(curr, val)
		periodIndices[i] = periodIndex
		if periodIndices[i] > maxPeriodIndex {
			maxPeriodIndex = periodIndices[i]
		}
		descriptions[i] = description
	}

	for i := range descriptions {
		// Add left padding to line up the descriptions real neat
		leftPaddingCount := maxPeriodIndex - periodIndices[i]
		description.WriteString(writeWithPadding(descriptions[i], leftPaddingCount))
		description.WriteByte('\n')
	}

	originalCurrencyDescription, _ := GetCurrencyDescription(query.OriginalCurrency, query.OriginalValue)
	response := fmt.Sprintf(
		"%s costs:\n%v",
		originalCurrencyDescription,
		&description,
	)
	return response
}

func Currency(command types.Command) types.Response {
	var response types.Response

	if len(command.Payload) == 0 {
		return response
	}

	currency, value, commandParsingError := parseArgs(command.Payload)
	if commandParsingError != nil {
		response.Text = commandParsingError.Error()
		return response
	}
	query := NewRatesQuery(currency, value)

	apiQueryError := query.doQuery()
	if apiQueryError != nil {
		response.Text = apiQueryError.Error()
		return response
	}

	responseDecodeError := query.decode()
	query.closeStreams()
	if responseDecodeError != nil {
		response.Text = "Failed to parse API response. Has either API changed?"
		return response
	}

	response.Text = query.getDescription()
	response.UseMarkdown = true
	response.DisableWebPagePreview = true
	return response
}
