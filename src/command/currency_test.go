package commands

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"gitlab.com/helm108/mawgobot/src/types"
	"io/ioutil"
	"testing"
)

func TestDecodeCurrencyRatesObject(t *testing.T) {
	var query RatesQuery
	query.CurrencyStream = ioutil.NopCloser(bytes.NewReader([]byte(`
	{
		"base":"USD",
		"rates":{
			"BGN":1.7523519398,
			"NZD":1.5358838814,
			"ILS":3.5992294597,
			"RUB":64.478630947,
			"CAD":1.3409192725,
			"USD":1.0,
			"PHP":52.5517426754,
			"CHF":1.0101245408,
			"AUD":1.4535435893,
			"JPY":110.4470925544,
			"TRY":6.0465012096,
			"HKD":7.8495654511,
			"MYR":4.1860048383,
			"HRK":6.6539736583,
			"CZK":23.0938087985,
			"IDR":14483.0033151151,
			"DKK":6.6916046949,
			"NOK":8.7727802168,
			"HUF":292.5544306066,
			"GBP":0.7849655049,
			"MXN":19.0700654063,
			"THB":31.9353104561,
			"ISK":123.4656392796,
			"ZAR":14.4170773228,
			"BRL":4.0935400054,
			"SGD":1.3781919183,
			"PLN":3.8580772332,
			"INR":69.6917838903,
			"KRW":1193.3339306514,
			"RON":4.2678075441,
			"CNY":6.9062807992,
			"SEK":9.6535256697,
			"EUR":0.895977063
		},
		"date":"2019-05-21"
	}`)))

	query.BitcoinStream = ioutil.NopCloser(bytes.NewReader([]byte(`
	{
		"time":{"updated":"May 27, 2019 19:36:00 UTC","updatedISO":"2019-05-27T19:36:00+00:00","updateduk":"May 27, 2019 at 20:36 BST"},
		"disclaimer":"This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org",
		"bpi":{
			"USD":{
				"code":"USD",
				"rate":"8,818.8433",
				"description":"United States Dollar",
				"rate_float":8818.8433
			}
		}
	}`)))

	err := query.decode()
	assert.Nil(t, err)
	assert.Equal(t, "USD", query.CurrencyRates.Base)
	assert.Equal(t, 9.6535256697, query.CurrencyRates.Rates["SEK"])
	assert.Equal(t, 9.6535256697, query.CombinedRates["SEK"])
	assert.Equal(t, "USD", query.BitcoinRate.BPI.USD.Code)
	assert.Equal(t, 8818.8433, query.BitcoinRate.BPI.USD.Rate)
}

func TestParseArgs(t *testing.T) {
	var err error
	var currency string
	var amount float64

	currency, amount, err = parseArgs("100 usd")
	assert.Nil(t, err)
	assert.Equal(t, "USD", currency)
	assert.Equal(t, 100.0, amount)

	currency, amount, err = parseArgs("$100")
	assert.Nil(t, err)
	assert.Equal(t, "USD", currency)
	assert.Equal(t, 100.0, amount)

	currency, amount, err = parseArgs("$100.00")
	assert.Nil(t, err)
	assert.Equal(t, "USD", currency)
	assert.Equal(t, 100.0, amount)

	currency, amount, err = parseArgs("100.00USD")
	assert.Nil(t, err)
	assert.Equal(t, "USD", currency)
	assert.Equal(t, 100.0, amount)

	currency, amount, err = parseArgs("100USD")
	assert.Nil(t, err)
	assert.Equal(t, "USD", currency)
	assert.Equal(t, 100.0, amount)

	currency, amount, err = parseArgs("100  \n \t USD")
	assert.Nil(t, err)
	assert.Equal(t, "USD", currency)
	assert.Equal(t, 100.0, amount)

	currency, amount, err = parseArgs("$100.00 GBP")
	assert.Nil(t, err)
	assert.Equal(t, "USD", currency)
	assert.Equal(t, 100.0, amount)

	currency, amount, err = parseArgs("100,000.00 SEK")
	assert.Nil(t, err)
	assert.Equal(t, "SEK", currency)
	assert.Equal(t, 100000.0, amount)

	currency, amount, err = parseArgs("100,000€")
	assert.Nil(t, err)
	assert.Equal(t, "EUR", currency)
	assert.Equal(t, 100000.0, amount)

	currency, amount, err = parseArgs("&$1")
	assert.Nil(t, err)
	assert.Equal(t, "USD", currency)
	assert.Equal(t, 1.0, amount)

	_, _, err = parseArgs("100 xyz")
	assert.NotNil(t, err)

	_, _, err = parseArgs("&100")
	assert.NotNil(t, err)

	_, _, err = parseArgs("USD 100")
	assert.NotNil(t, err)

	_, _, err = parseArgs("")
	assert.NotNil(t, err)

	_, _, err = parseArgs("100")
	assert.NotNil(t, err)
}

func TestGetSymbol(t *testing.T) {
	var symbol string
	var err error
	var symbolPrecedesNumber bool

	symbol, symbolPrecedesNumber, err = getSymbol("USD")
	assert.Nil(t, err)
	assert.Equal(t, "$", symbol)
	assert.True(t, symbolPrecedesNumber)

	symbol, symbolPrecedesNumber, err = getSymbol("GBP")
	assert.Nil(t, err)
	assert.Equal(t, "£", symbol)
	assert.True(t, symbolPrecedesNumber)

	_, _, err = getSymbol("XYZ")
	assert.NotNil(t, err)

	symbol, symbolPrecedesNumber, err = getSymbol("EUR")
	assert.Nil(t, err)
	assert.Equal(t, "€", symbol)
	assert.False(t, symbolPrecedesNumber)
}

func TestGetCurrency(t *testing.T) {
	var curr string
	var err error

	curr, err = getCurrency("$")
	assert.Nil(t, err)
	assert.Equal(t, "USD", curr)

	curr, err = getCurrency("£")
	assert.Nil(t, err)
	assert.Equal(t, "GBP", curr)

	_, err = getCurrency("&")
	assert.NotNil(t, err)

	curr, err = getCurrency("€")
	assert.Nil(t, err)
	assert.Equal(t, "EUR", curr)
}

func TestIsValidCurrency(t *testing.T) {
	assert.True(t, isValidCurrency("USD"))
	assert.True(t, isValidCurrency("SEK"))
	assert.False(t, isValidCurrency("XYZ"))
	assert.False(t, isValidCurrency(""))
	assert.False(t, isValidCurrency("$"))
	assert.True(t, isValidCurrency("NOK"))
	assert.True(t, isValidCurrency("ISK"))
	assert.True(t, isValidCurrency("GBP"))
	assert.False(t, isValidCurrency("YEN"))
}

func TestBuildCurrencyDescription(t *testing.T) {
	var query RatesQuery
	query.CurrencyRates = types.CurrencyRates{
		Base:  "NOK",
		Rates: make(map[string]float64),
	}
	query.CurrencyRates.Rates["SEK"] = 1.00
	query.CurrencyRates.Rates["USD"] = 0.50
	query.CurrencyRates.Rates["EUR"] = 1000.0
	query.CurrencyRates.Rates["GBP"] = 0.05
	query.CurrencyRates.Rates["ISK"] = 1.00
	query.DerivedValue = 100.00

	text := query.getDescription()
	assert.Contains(t, text, "100.00 SEK")
	assert.Contains(t, text, "$50.00")
	assert.Contains(t, text, "100,000.00€")
	assert.Contains(t, text, "£5.00")
	assert.NotContains(t, text, "ISK") // not a currency of interest
}

func TestGetRepresentation(t *testing.T) {
	var description string
	var periodIndex int

	description, periodIndex = GetCurrencyDescription("SEK", 100.0)
	assert.Equal(t, "100.00 SEK", description)
	assert.Equal(t, 3, periodIndex)

	description, periodIndex = GetCurrencyDescription("NOK", 10.0)
	assert.Equal(t, "10.00 NOK", description)
	assert.Equal(t, 2, periodIndex)

	description, periodIndex = GetCurrencyDescription("USD", 25.0)
	assert.Equal(t, "$25.00", description)
	assert.Equal(t, 3, periodIndex)

	description, periodIndex = GetCurrencyDescription("GBP", 10000.0)
	assert.Equal(t, "£10,000.00", description)
	assert.Equal(t, 7, periodIndex)

	description, periodIndex = GetCurrencyDescription("EUR", 1.0)
	assert.Equal(t, "1.00€", description)
	assert.Equal(t, 1, periodIndex)
}

func TestGetThousandsSeparatedNumber(t *testing.T) {
	var number string
	var groupVal int

	groupVal, number = getThousandsSeparatedNumber(".21")
	assert.Equal(t, ".21", number)
	assert.Equal(t, 0, groupVal)

	groupVal, number = getThousandsSeparatedNumber("3.21")
	assert.Equal(t, "3.21", number)
	assert.Equal(t, 1, groupVal)

	groupVal, number = getThousandsSeparatedNumber("43.21")
	assert.Equal(t, "43.21", number)
	assert.Equal(t, 2, groupVal)

	groupVal, number = getThousandsSeparatedNumber("543.21")
	assert.Equal(t, "543.21", number)
	assert.Equal(t, 3, groupVal)

	groupVal, number = getThousandsSeparatedNumber("6543.21")
	assert.Equal(t, "6,543.21", number)
	assert.Equal(t, 1, groupVal)

	groupVal, number = getThousandsSeparatedNumber("76543.21")
	assert.Equal(t, "76,543.21", number)
	assert.Equal(t, 2, groupVal)

	groupVal, number = getThousandsSeparatedNumber("876543.21")
	assert.Equal(t, "876,543.21", number)
	assert.Equal(t, 3, groupVal)

	groupVal, number = getThousandsSeparatedNumber("9876543.21")
	assert.Equal(t, "9,876,543.21", number)
	assert.Equal(t, 1, groupVal)

	groupVal, number = getThousandsSeparatedNumber("10000.00")
	assert.Equal(t, "10,000.00", number)
	assert.Equal(t, 2, groupVal)

	groupVal, number = getThousandsSeparatedNumber("10.00")
	assert.Equal(t, "10.00", number)
	assert.Equal(t, 2, groupVal)

	groupVal, number = getThousandsSeparatedNumber("1.00")
	assert.Equal(t, "1.00", number)
	assert.Equal(t, 1, groupVal)

	groupVal, number = getThousandsSeparatedNumber("12345.67")
	assert.Equal(t, "12,345.67", number)
	assert.Equal(t, 2, groupVal)

	groupVal, number = getThousandsSeparatedNumber("12345678.90")
	assert.Equal(t, "12,345,678.90", number)
	assert.Equal(t, 2, groupVal)

	groupVal, number = getThousandsSeparatedNumber("")
	assert.Equal(t, "", number)
	assert.Equal(t, 0, groupVal)

	groupVal, number = getThousandsSeparatedNumber("1")
	assert.Equal(t, "1", number)
	assert.Equal(t, 1, groupVal)

	groupVal, number = getThousandsSeparatedNumber("12")
	assert.Equal(t, "12", number)
	assert.Equal(t, 2, groupVal)

	groupVal, number = getThousandsSeparatedNumber("123")
	assert.Equal(t, "123", number)
	assert.Equal(t, 3, groupVal)

	groupVal, number = getThousandsSeparatedNumber("1234")
	assert.Equal(t, "1,234", number)
	assert.Equal(t, 1, groupVal)
}
