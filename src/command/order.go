package commands

import (
	"gitlab.com/helm108/mawgobot/src/types"
	"math/rand"
	"time"
)

func getOrderProclamation() string {
	var proclamations = [...]string{
		"_Order!_",
		"*ORDER!*",
		"Order, order!",
		"Order. *ORDER!*",
		"Order! _Order!_",
		"Order! Order! *Order!*",
		"ORDAHH",
		"_ORDAHHHHH!_",
		"*OOOOORRRDAAAAAAHHHH!!*",
	}
	rand.Seed(time.Now().UTC().UnixNano())
	indexToReturn := rand.Intn(len(proclamations))
	return proclamations[indexToReturn]
}

func Order(command types.Command) types.Response {
	var response types.Response
	response.Text = getOrderProclamation()
	response.IsBroadcast = true
	response.UseMarkdown = true
	return response
}
