package commands

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestEventuallyGetDifferentProclamationForRandomSeeds(t *testing.T) {
	var i int
	for i = 0; i < 100; i++ {
		proclamation1 := getOrderProclamation()
		proclamation2 := getOrderProclamation()
		if proclamation1 == proclamation2 {
			break
		}
	}
	if i == 99 {
		// We tried 100 times and never got two different proclamations
		// The chance for this happening with fair random seed generation, is
		// (1/x)^100, where x is the number of options. For x = 7, this gives 10^-85
		assert.FailNow(t, "Different seeds should (eventually) get different proclamations")
	}
}
