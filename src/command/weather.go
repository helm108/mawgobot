package commands

import (
	"encoding/json"
	"fmt"
	"gitlab.com/helm108/mawgobot/src/types"
	"io"
	"net/http"
	"os"
)

const WeatherAPI = "https://api.openweathermap.org/data/2.5"

var weatherCodeEmojis = map[int]string{
	200: "⛈",  // thunderstorm with light rain
	201: "⛈",  // thunderstorm with rain
	202: "⛈",  // thunderstorm with heavy rain
	210: "🌩",  // light thunderstorm
	211: "🌩",  // thunderstorm
	212: "🌩",  // heavy thunderstorm
	221: "🌩",  // ragged thunderstorm
	230: "⛈",  // thunderstorm with light drizzle
	231: "⛈",  // thunderstorm with drizzle
	232: "⛈",  // thunderstorm with heavy drizzle
	300: "🌧",  // intensity drizzle
	301: "🌧",  // drizzle
	302: "🌧",  // heavy intensity drizzle
	310: "🌧",  // light intensity drizzle rain
	311: "🌧",  // drizzle rain
	312: "🌧",  // heavy intensity drizzle rain
	313: "🌧",  // shower rain and drizzle
	314: "🌧",  // heavy shower rain and drizzle
	321: "🌧",  // shower drizzle
	500: "🌧",  // light rain
	501: "🌧",  // moderate rain
	502: "🌧",  // heavy intensity rain
	503: "🌧",  // very heavy rain
	504: "🌧",  // extreme rain
	511: "🌧",  // freezing rain
	520: "🌧",  // light intensity shower rain
	521: "🌧",  // shower rain
	522: "🌧",  // heavy intensity shower rain
	531: "🌧",  // ragged shower rain
	600: "🌨",  // light snow
	601: "🌨",  // snow
	602: "🌨",  // heavy snow
	611: "🌨",  // sleet
	612: "🌨",  // light shower sleet
	613: "🌨",  // shower sleet
	615: "🌨",  // light rain and snow
	616: "🌨",  // rain and snow
	620: "🌨",  // light shower snow
	621: "🌨",  // shower snow
	622: "🌨",  // heavy shower snow
	701: "🌫️", // mist
	711: "🌫️", // smoke
	721: "🌫️", // haze
	731: "🌫️", // sand/dust whirls
	741: "🌁",  // fog
	751: "🌫️", // sand
	761: "🌫️", // dust
	762: "🌋",  // volcanic ash
	771: "🌬️", // squalls
	781: "🌪️", // tornado
	800: "☀️", // clear
	801: "🌤️", // few clouds: 11-25%
	802: "⛅",  // scattered clouds: 25-50%
	803: "🌥️", // broken clouds: 51-84%
	804: "☁️", // overcast clouds: 85-100%
}

func queryOWMForWeather(location string) (io.ReadCloser, error) {
	url := fmt.Sprintf(
		"%s/weather?q=%s&APPID=%s",
		WeatherAPI,
		location,
		os.Getenv("OWM_API_TOKEN"),
	)

	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	return resp.Body, nil
}

func decodeWeatherObject(textStream io.Reader, weatherObject *types.WeatherObject) error {
	decoder := json.NewDecoder(textStream)
	err := decoder.Decode(weatherObject)
	return err
}

func getLocationDescription(weatherObject *types.WeatherObject) string {
	gmaps_url := fmt.Sprintf(
		"%s%s,%s",
		"https://www.google.com/maps/search/?api=1&query=",
		weatherObject.Coord.Lat.String(),
		weatherObject.Coord.Lon.String(),
	)
	description := fmt.Sprintf(
		"[%s, %s](%s)",
		weatherObject.Name,
		weatherObject.Sys.Country,
		gmaps_url,
	)
	return description
}

func getWeatherDescription(weatherObject *types.WeatherObject) string {
	// 0°C = 273.15 K
	tempInDegC := weatherObject.Main.Temp - 273.15
	minTempInDegC := weatherObject.Main.Temp_min - 273.15
	maxTempInDegC := weatherObject.Main.Temp_max - 273.15
	weatherDescription := fmt.Sprintf(
		"%.1f°C, %s %s\n(%.1f°C | %.1f°C)",
		tempInDegC,
		weatherObject.Weather[0].Description,
		weatherCodeEmojis[weatherObject.Weather[0].Id],
		minTempInDegC,
		maxTempInDegC,
	)
	return weatherDescription
}

func parseWeatherObject(location string, weatherObject *types.WeatherObject) string {
	if weatherObject.Cod == "404" {
		return fmt.Sprintf("I've never heard of %s", location)
	}

	if weatherObject.Cod != "200" {
		return fmt.Sprintf(
			"The weather service is currently unavailable (`%s`)",
			weatherObject.Cod,
		)
	}

	return fmt.Sprintf(
		"%s\n%s",
		getLocationDescription(weatherObject),
		getWeatherDescription(weatherObject),
	)
}

func Weather(command types.Command) types.Response {
	var weatherObject types.WeatherObject
	var response types.Response

	if len(command.Payload) == 0 {
		return response
	}

	location := command.Payload
	replyStream, err := queryOWMForWeather(location)
	if err != nil {
		response.Text = fmt.Sprintf("Unable to connect to `%s`", WeatherAPI)
		return response
	}

	err = decodeWeatherObject(replyStream, &weatherObject)
	defer replyStream.Close()
	if err != nil {
		response.Text = "Unable to parse OWM's data object, has the API changed?"
		return response
	}

	response.Text = parseWeatherObject(location, &weatherObject)
	response.UseMarkdown = true
	response.DisableWebPagePreview = true
	return response
}
