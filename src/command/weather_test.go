package commands

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/helm108/mawgobot/src/types"
	"testing"
)

func TestWeatherReturnsNothingIfPayloadEmpty(t *testing.T) {
	command := types.Command{
		Command: "weather",
		Payload: "",
	}
	response := Weather(command)

	assert.Empty(t, response.Text)
}

func TestGetLocationDescription(t *testing.T) {
	weatherObject := &types.WeatherObject{
		Name: "Hófsos",
		Sys: types.Sys_info{
			Country: "IS",
		},
		Coord: types.Coords_info{
			Lon: json.Number("42.42"),
			Lat: json.Number("-42.42"),
		},
	}

	description := getLocationDescription(weatherObject)
	assert.Equal(
		t,
		"[Hófsos, IS](https://www.google.com/maps/search/?api=1&query=-42.42,42.42)",
		description,
	)
}

func TestGetWeatherDescription(t *testing.T) {
	weatherObject := &types.WeatherObject{
		Main: types.Main_info{
			Temp:     333.33, // 60.18 deg C
			Temp_min: 332.22, // 59.07 deg C
			Temp_max: 334.44, // 61.29 deg C
		},
		Weather: []types.Weather_info{
			{
				Description: "Cloudy with a chance of meatballs",
				Id:          762,
			},
		},
	}

	description := getWeatherDescription(weatherObject)
	assert.Equal(t, "60.2°C, Cloudy with a chance of meatballs 🌋\n(59.1°C | 61.3°C)", description)
}

func TestParse200WeatherObject(t *testing.T) {
	weatherObject := &types.WeatherObject{
		Cod:  "200",
		Name: "Hófsos",
		Sys: types.Sys_info{
			Country: "IS",
		},
		Coord: types.Coords_info{
			Lon: json.Number("42.42"),
			Lat: json.Number("-42.42"),
		},
		Main: types.Main_info{
			Temp:     333.33, // 60.18 deg C
			Temp_min: 332.22, // 59.07 deg C
			Temp_max: 334.44, // 61.29 deg C
		},
		Weather: []types.Weather_info{
			{
				Description: "Cloudy with a chance of meatballs",
				Id:          762,
			},
		},
	}

	expected := fmt.Sprintf(
		"%s\n%s",
		"[Hófsos, IS](https://www.google.com/maps/search/?api=1&query=-42.42,42.42)",
		"60.2°C, Cloudy with a chance of meatballs 🌋\n(59.1°C | 61.3°C)",
	)
	description := parseWeatherObject("anything", weatherObject)
	assert.Equal(t, expected, description)
}

func TestParse404WeatherObject(t *testing.T) {
	weatherObject := &types.WeatherObject{
		Cod: "404",
	}

	description := parseWeatherObject("Nangiala", weatherObject)
	assert.Equal(t, "I've never heard of Nangiala", description)
}

func TestParse405WeatherObject(t *testing.T) {
	weatherObject := &types.WeatherObject{
		Cod: "405",
	}

	description := parseWeatherObject("Nangiala", weatherObject)
	assert.Equal(
		t,
		"The weather service is currently unavailable (`405`)",
		description,
	)
}

func TestDecodeWeatherObjectLondon(t *testing.T) {
	var weatherObject types.WeatherObject
	jsonStream := bytes.NewReader([]byte(`{
		"coord":{"lon":-0.13,"lat":51.51},
		"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],
		"base":"stations",
		"main":{"temp":287.1,"pressure":1006,"humidity":63,"temp_min":284.82,"temp_max":289.26},
		"visibility":10000,"wind":{"speed":1},
		"clouds":{"all":75},
		"dt":1558172744,
		"sys":{"type":1,"id":1414,"message":0.0153,"country":"GB","sunrise":1558152298,"sunset":1558208948},
		"id":2643743,
		"name":"London",
		"cod":200
	}`))

	err := decodeWeatherObject(jsonStream, &weatherObject)
	assert.Nil(t, err)
	assert.Equal(t, "London", weatherObject.Name)
}

func TestDecodeWeatherObjectHófsos(t *testing.T) {
	var weatherObject types.WeatherObject
	jsonStream := bytes.NewReader([]byte(`{
		"name":"Hófsos"
	}`))
	err := decodeWeatherObject(jsonStream, &weatherObject)
	assert.Nil(t, err)
	assert.Equal(t, "Hófsos", weatherObject.Name)
}

func TestDecodeWeatherObjectWithEmptyResult(t *testing.T) {
	var weatherObject types.WeatherObject
	jsonStream := bytes.NewReader([]byte(`{
		"nam":"Hófsos"
	}`))
	err := decodeWeatherObject(jsonStream, &weatherObject)
	assert.Nil(t, err)
	assert.Equal(t, "", weatherObject.Name)
}

func TestDecodeWeatherObjectWithBadJSON(t *testing.T) {
	var weatherObject types.WeatherObject
	jsonStream := bytes.NewReader([]byte(`{
		{{{ : dad are you JSON
		{{{ : yes son now we can be family again
	}`))
	err := decodeWeatherObject(jsonStream, &weatherObject)
	assert.NotNil(t, err)
}
