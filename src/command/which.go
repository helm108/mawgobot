package commands

import (
	"gitlab.com/helm108/mawgobot/src/types"
	"math/rand"
	"strings"
	"time"
)

// Returns an int in the range 0 to "max minus one".
func randomIntInRange(max int) int {
	randSeed := rand.NewSource(time.Now().Unix())
	rng := rand.New(randSeed)
	return rng.Intn(max)
}

func SplitPayload(payload string) []string {
	// If string contains commas, split by comma.
	if strings.Index(payload, ",") != -1 {
		options := strings.Split(payload, ",")

		// Trim comma-separated options.
		for index, element := range options {
			options[index] = strings.Trim(element, " ")
		}

		return options
	}

	// Else, split by spaces.
	return strings.Split(payload, " ")
}

func Which(command types.Command) types.Response {
	var response types.Response

	if command.Payload == "" {
		return response
	}

	options := SplitPayload(command.Payload)

	randomIndex := randomIntInRange(len(options))

	response.Text = options[randomIndex]

	return response
}
