package commands

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/helm108/mawgobot/src/types"
	"testing"
)

func TestReturnsNothingIfPayloadEmpty(t *testing.T) {
	command := types.Command{
		Command: "which",
		Payload: "",
	}
	response := Which(command)

	assert.Empty(t, response.Text)
}

func TestResponseContainsOnlyOptions(t *testing.T) {
	command := types.Command{
		Command: "which",
		Payload: "one",
	}
	response := Which(command)

	assert.Equal(t, "one", response.Text)
}

func TestSplitsPayloadByCommas(t *testing.T) {
	payload := "first choice, second choice, third choice"

	result := SplitPayload(payload)

	assert.Equal(t, "first choice", result[0])
	assert.Equal(t, "second choice", result[1])
	assert.Equal(t, "third choice", result[2])
}

func TestSplitsPayloadBySpaceIfNoCommas(t *testing.T) {
	payload := "first second third"

	result := SplitPayload(payload)

	assert.Equal(t, "first", result[0])
	assert.Equal(t, "second", result[1])
	assert.Equal(t, "third", result[2])
}
