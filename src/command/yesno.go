package commands

import (
	"crypto/md5"
	"encoding/binary"
	"gitlab.com/helm108/mawgobot/src/types"
	"io"
	"math/rand"
)

const invalidResponse string = "That is not a question."
const yes string = "Yes."
const no string = "No."
const hmm string = "Hmm."

// Convert a string to an int to be used as a rand seed.
// https://stackoverflow.com/questions/48307105/
func stringToUint64(str string) uint64 {
	hash := md5.New()
	io.WriteString(hash, str)
	return binary.BigEndian.Uint64(hash.Sum(nil))
}

func getRandomFloatFromString(payload string) float32 {
	seed := stringToUint64(payload)
	rand.Seed(int64(seed))
	return rand.Float32()
}

func ChooseAnswer(randomNumber float32) string {
	if randomNumber < 0.49 {
		return yes
	}

	if randomNumber >= 0.49 && randomNumber <= 0.98 {
		return no
	}

	return hmm
}

func YesNo(command types.Command) types.Response {
	var response types.Response

	if command.Payload == "" || command.Payload == "?" {
		response.Text = invalidResponse
		return response
	}

	lastChar := command.Payload[len(command.Payload)-1:]
	if lastChar != "?" {
		response.Text = invalidResponse
		return response
	}

	randomNumber := getRandomFloatFromString(command.Payload)
	response.Text = ChooseAnswer(randomNumber)

	return response
}
