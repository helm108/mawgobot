package commands

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/helm108/mawgobot/src/types"
	"testing"
)

func TestReturnsErrorIfPayloadIsNotAQuestion(t *testing.T) {
	command := types.Command{
		Command: "mrspeaker",
		Payload: "test question",
	}

	response := YesNo(command)

	assert.Equal(t, "That is not a question.", response.Text)
}

func TestReturnsErrorIfPayloadIsEmpty(t *testing.T) {
	command := types.Command{
		Command: "mrspeaker",
		Payload: "",
	}

	response := YesNo(command)

	assert.Equal(t, "That is not a question.", response.Text)
}

func TestReturnsErrorIfPayloadIsOnlyAQuestionMark(t *testing.T) {
	command := types.Command{
		Command: "mrspeaker",
		Payload: "?",
	}

	response := YesNo(command)

	assert.Equal(t, "That is not a question.", response.Text)
}

func TestReturnsYesIf0(t *testing.T) {
	answer := ChooseAnswer(0.0)

	assert.Equal(t, "Yes.", answer)
}

func TestReturnsYesIf048(t *testing.T) {
	answer := ChooseAnswer(0.48)

	assert.Equal(t, "Yes.", answer)
}

func TestReturnsNoIf049(t *testing.T) {
	answer := ChooseAnswer(0.49)

	assert.Equal(t, "No.", answer)
}

func TestReturnsNoIf098(t *testing.T) {
	answer := ChooseAnswer(0.98)

	assert.Equal(t, "No.", answer)
}

func TestReturnsHmmIf099(t *testing.T) {
	answer := ChooseAnswer(0.99)

	assert.Equal(t, "Hmm.", answer)
}

func TestReturnsHmmIf100(t *testing.T) {
	answer := ChooseAnswer(1.0)

	assert.Equal(t, "Hmm.", answer)
}
