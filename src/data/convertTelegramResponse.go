package data

import (
	"encoding/json"
	"gitlab.com/helm108/mawgobot/src/types"
)

func ParseJson(str string) types.TelegramResponse {
	res := types.TelegramResponse{}
	json.Unmarshal([]byte(str), &res)
	return res
}
