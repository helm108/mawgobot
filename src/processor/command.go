package processor

import (
	commands "gitlab.com/helm108/mawgobot/src/command"
	"gitlab.com/helm108/mawgobot/src/types"
)

func ProcessCommand(command types.Command) types.Response {
	var response types.Response

	switch command.Command {
	case "which":
		return commands.Which(command)
	case "weather":
		return commands.Weather(command)
	case "mrspeaker":
		return commands.YesNo(command)
	case "convert":
		return commands.Currency(command)
	case "order":
		return commands.Order(command)
	}

	return response
}
