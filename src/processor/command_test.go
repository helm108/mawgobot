package processor

import (
	"gitlab.com/helm108/mawgobot/src/types"
	"testing"
)

func TestProcessCommandDetectsWhich(t *testing.T) {
	command := types.Command{
		Command: "which",
		Payload: "one two three",
	}

	ProcessCommand(command)
}
