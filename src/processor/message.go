package processor

import (
	"gitlab.com/helm108/mawgobot/src/types"
	"strings"
)

// Clean the command of any extraneous characters.
func cleanCommand(command string) string {
	forbiddenCharacters := []string{
		",",
	}

	for i := range forbiddenCharacters {
		forbiddenCharacter := forbiddenCharacters[i]
		command = strings.Replace(command, forbiddenCharacter, "", -1)
	}

	return command
}

// Split the received message into Command and Payload segments.
func ConvertTextToCommand(text string) types.Command {
	var command types.Command

	// Confirm string starts with a /.
	if strings.Index(text, "/") != 0 {
		return command
	}

	// Remove slash.
	text = strings.Replace(text, "/", "", 1)

	// Split string.
	components := strings.Split(text, " ")

	// Get command.
	command.Command = cleanCommand(components[0])
	command.Payload = strings.Join(components[1:], " ")

	return command
}

// Function names must be unique per namespace, not per file.
func ProcessTelegramResponse(telegramResponse types.TelegramResponse) types.Command {
	text := telegramResponse.Message.Text
	return ConvertTextToCommand(text)
}
