package processor

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/helm108/mawgobot/src/types"
	"testing"
)

func TestNotACommand(t *testing.T) {
	actual := ConvertTextToCommand("which one two three")

	assert.Equal(t, "", actual.Command)
	assert.Equal(t, "", actual.Payload)
}

func TestValidCommandWithPayload(t *testing.T) {
	expected := types.Command{Command: "which", Payload: "one two three"}
	actual := ConvertTextToCommand("/which one two three")

	if expected != actual {
		t.Error("Test failed")
	}
}

func TestAllowsCommandAfterCommand(t *testing.T) {
	expected := types.Command{Command: "mrspeaker", Payload: "am I a bike"}
	actual := ConvertTextToCommand("/mrspeaker, am I a bike")

	assert.Equal(t, expected, actual)
}
