package processor

import (
	"gitlab.com/helm108/mawgobot/src/data"
	"gitlab.com/helm108/mawgobot/src/types"
)

func ProcessRequestBody(body string) types.TelegramResponse {
	return data.ParseJson(body)
}
