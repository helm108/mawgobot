package types

type CurrencyRates struct {
	Base  string             `json:"base"`
	Rates map[string]float64 `json:"rates"`
}

type BitcoinRate struct {
	BPI BPI `json:"bpi"`
}

type BPI struct {
	USD BitcoinUSDRate `json:"USD"`
}

type BitcoinUSDRate struct {
	Code string  `json:"code"`
	Rate float64 `json:"rate_float"`
}
