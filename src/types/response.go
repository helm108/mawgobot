package types

type Response struct {
	Text                  string
	UseMarkdown           bool
	DisableWebPagePreview bool
	IsBroadcast           bool
}
