package types

import (
	"encoding/json"
)

type WeatherObject struct {
	Coord      Coords_info
	Weather    []Weather_info
	Base       string
	Main       Main_info
	Visibility int
	Wind       Wind_info
	Clouds     Clouds_info
	Dt         int
	Sys        Sys_info
	Id         int
	Name       string
	Cod        json.Number
}

type Coords_info struct {
	Lon json.Number
	Lat json.Number
}

type Weather_info struct {
	Id          int
	Main        string
	Description string
	icon        string
}

type Main_info struct {
	Temp     float64
	Pressure float64
	Humidity int
	Temp_min float64
	Temp_max float64
}

type Wind_info struct {
	Speed float64
	Deg   float64
}

type Clouds_info struct {
	All int
}

type Sys_info struct {
	Type    int
	Id      int
	Message json.Number
	Country string
	Sunrise int
	Sunset  int
}
