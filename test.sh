#!/usr/bin/env bash
echo ""
echo "Running Tests"
echo ""
go test ./... -coverprofile=c.out

echo ""
echo "Test Coverage"
echo ""
go tool cover -func=c.out
